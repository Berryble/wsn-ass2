# ASSIGNMENT 2

---
## OVERVIEW

I've implemented 3 types of DCT:
1.  *Naive DCT* - Simply implements the formula and does no optimization
2.  *Faster DCT* - Cuts the cycles required by at least a half by utilizing the cosine symmetry 
3.  *LLM DCT* - 8-point DCT approach as described in https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.463.3353&rep=rep1&type=pdf

---

## RESULTS

|  N  	|  M  	|  TIME 	|    POWER[^1]  |    MSE    	| ALGORITHM 	|
|:---:	|:---:	|:-----:	|:-----------:	|:---------:	|:---------:	|
| 256 	|  75 	|  216s 	| 1399.946 mJ 	| 0.0001916 	|   Naive   	|
| 256 	|  75 	|  103s 	|  671.411 mJ 	| 0.0001916 	|   Faster  	|
| 256 	| 256 	|  <1s  	|   2.527 mJ  	|  5.64e-07 	|    LLM    	|
| 512 	| 120 	| 1251s 	| 8112.452 mJ 	|  0.001307 	|   Naive   	|
| 512 	| 120 	|  339s 	| 2196.202 mJ 	|  0.001307 	|   Faster  	|
| 512 	| 512 	|  ~1s  	|   5.73 mJ   	| 0.0002403 	|    LLM    	|

[^1]: The following formula was used when calculating power (Only CPU power was calculated as there are no RX / TX operations):
![img.png](verifier/imgs/pow_func.png)

---
Here are the differences visualized (For 256:75 case):

**NAIVE and FASTER:**

![img.png](verifier/imgs/naive.png)

**LLM:**

![img.png](verifier/imgs/llm.png)


**NOTE:** For LLM algorithm, I calculated all coefficients due to the fact that it is an 8-point algorithm meaning that it calculates 8-coefficients at a time and reconstructs the signal 8-points at a time (thus calculating only 75 / 120 coefficients can be tedious). However, as the last few coefficients aren't very important (as the coefficients at index 6, 7 and 8 tend to be very close to zero) the calculation could be skipped entirely in favor of even faster execution.

---
## SIGNAL GENERATION & RESULT VERIFICATION 

Firstly, the signal was generated using the [SciPy's electrocardiogram library](https://docs.scipy.org/doc/scipy/reference/generated/scipy.misc.electrocardiogram.html) after transforming the signal it was reconstructed using [SciPy's DCT library](https://docs.scipy.org/doc/scipy/reference/generated/scipy.fftpack.dct.html).

The results defined in the Results section can be easily recreated and re-verified by performing the following:
1. Set ``LENGTH_N`` and ``LENGTH_M`` variables in ``dct.c`` (``LENGTH_N`` *can only be either **256** or **512***):
```c
#define LENGTH_N 256 // (LINE 9 in dct.c)
#define LENGTH_M 75
```
2. Execute the following command in the directory of this project:
```sh
   make TARGET=sky && make TARGET=sky MOTES=/dev/ttyUSB0 dct.upload login # You might need to change the port from /dev/USB0 to something else (can find it by performing ls /dev/)
```
3. Wait until all algorithms are executed (This varies from ~24 minutes to ~5 minutes depending on which N is being used):
```
----- SAMPLE OUTPUT -----
NAIVE DCT TIME: 1251
POWER CONSUMPTION: 8112.452 mJ
COEFFICIENTS: [<coefficients>]
// ========================= //

// ======= FASTER DCT ======= //
FASTER DCT TIME: 339
POWER CONSUMPTION: 2196.202 mJ
COEFFICIENTS: [<coefficients>]
// ========================= //

// ========= LLM DCT ======== //
LLM DCT TIME: 1
POWER CONSUMPTION: 5.73 mJ
COEFFICIENTS: [<coefficients>]
// ========================= //
```   

4. Replace either line 79 or  line 86 for naive / faster coefficients (or lines 19 / 38 for LLM coefficients) in ``verifier/verification_script.py`` with the output coefficients:
```python

######################################################################
##                            LLM DCT                               ##
######################################################################
# If N=256 (Line 19)
llm_dct = [<YOUR OUTPUT COEFFICIENTS>]

# If N=512 (Line 38)
llm_dct_512 = [<YOUR OUTPUT COEFFICIENTS]

######################################################################
##                          NAIVE OR FASTER                         ##
######################################################################
# If N=256: (Line 79)
faster_256_70[0:75] = [<YOUR OUTPUT COEFFICIENTS (Either Naive or Faster)>]

# If N=512: (Line 86)
faster_512_120[0:120] = [<YOUR OUTPUT COEFFICIENTS (Either Naive or Faster)>]
```
5. Run the python script to get the MSE of the algorithms as well as visualisation of the signals
``` 
---- SAMPLE OUTPUT ----
MSE of 8-bit LLM DCT (n=256; m=256): 5.645515411975251e-07
MSE of 8-bit LLM DCT (n=512; m=512): 0.00024036691608850033
MSE of NAIVE / FASTER DCT (256; 75): 0.00019160116855333647
MSE of NAIVE / FASTER DCT (512; 120): 0.0013074771488571124
```


**NOTE:** In order to run the python script you'll need to install the required libraries which can be found in ``verifier/requirements.txt``




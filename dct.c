// I am fully aware that this could be written much better and cleaner, however I don't believe that is the point of the exercise
// thus nasty code it is, blame contiki for having weird Makefile configuration (CMake > Makefile :|).
#include "contiki.h"
#include <math.h>
#include <stdlib.h>
#include "sys/energest.h"
#include <stdio.h> /* For printf() */

#define LENGTH_N 256 // The supported values are 512 and 256
#define LENGTH_M 75
#define CURRENT 1.8 // As seen in https://www.willow.co.uk/TelosB_Datasheet.pdf
#define VOLTAGE 3.6  // As seen in http://www2.ece.ohio-state.edu/~bibyk/ee582/telosMote.pdf

// DCT FUNCTIONS

void dct_slow(int n, int m);

void faster_dct(int n, int m);

void llm(int i);

void small_dct(int n);

// PRINT FUNCTIONS

void print_double(double val);

void print_coeffs(int m);

void print_power_consumption(long unsigned int cpu_pre, long unsigned int cpu_post);

// SIGNAL (TAKEN FROM:  https://docs.scipy.org/doc/scipy/reference/generated/scipy.misc.electrocardiogram.html)

#if LENGTH_N == 256
double ECG[256] = {-0.245, -0.215, -0.185, -0.175, -0.17, -0.17, -0.185, -0.17, -0.16, -0.15, -0.17, -0.205, -0.22,
                   -0.23,
                   -0.21, -0.19, -0.175, -0.185, -0.19, -0.19, -0.2, -0.2, -0.21, -0.205, -0.215, -0.205, -0.225,
                   -0.235,
                   -0.225, -0.205, -0.21, -0.2, -0.2, -0.195, -0.185, -0.19, -0.215, -0.22, -0.195, -0.195, -0.22,
                   -0.25,
                   -0.22, -0.165, -0.15, -0.145, -0.185, -0.195, -0.195, -0.185, -0.17, -0.16, -0.16, -0.18, -0.205,
                   -0.2,
                   -0.165, -0.135, -0.13, -0.13, -0.13, -0.11, -0.065, -0.03, -0.04, -0.055, -0.055, -0.035, 0.0, 0.015,
                   0.015, 0.0, 0.01, 0.015, 0.02, 0.035, 0.045, 0.04, 0.03, 0.025, 0.005, -0.005, -0.025, -0.05, -0.05,
                   -0.045, -0.025, -0.02, -0.035, -0.065, -0.065, -0.075, -0.07, -0.085, -0.1, -0.14, -0.145, -0.14,
                   -0.115,
                   -0.095, -0.09, -0.1, -0.11, -0.125, -0.125, -0.11, -0.085, -0.095, -0.08, -0.065, -0.03, -0.04,
                   -0.065,
                   -0.11, -0.145, -0.145, -0.075, 0.035, 0.22, 0.435, 0.695, 1.005, 1.3, 1.535, 1.72, 1.82, 1.72, 1.425,
                   1.02, 0.6, 0.2, -0.095, -0.215, -0.19, -0.125, -0.105, -0.115, -0.13, -0.1, -0.075, -0.08, -0.11,
                   -0.125, -0.155, -0.165, -0.17, -0.17, -0.17, -0.17, -0.185, -0.185, -0.2, -0.19, -0.175, -0.155,
                   -0.14, -0.175, -0.175, -0.17, -0.15, -0.16, -0.165, -0.18, -0.19, -0.165, -0.14, -0.13, -0.15, -0.16,
                   -0.145, -0.125, -0.115, -0.11, -0.115, -0.12, -0.115, -0.095, -0.085, -0.09, -0.09, -0.1, -0.095,
                   -0.085,
                   -0.065, -0.06, -0.07, -0.05, -0.045, -0.02, 0.01, 0.02, 0.04, 0.055, 0.065, 0.075, 0.095, 0.115,
                   0.085,
                   0.06, 0.08, 0.125, 0.16, 0.165, 0.17, 0.185, 0.22, 0.225, 0.235, 0.22, 0.23, 0.25, 0.28, 0.27, 0.25,
                   0.255, 0.245, 0.235, 0.235, 0.25, 0.23, 0.245, 0.23, 0.22, 0.215, 0.18, 0.12, 0.075, 0.065, 0.06,
                   0.055,
                   0.025, -0.005, -0.04, -0.085, -0.135, -0.145, -0.14, -0.12, -0.12, -0.16, -0.195, -0.2, -0.215, -0.2,
                   -0.2, -0.215, -0.255, -0.27, -0.225, -0.225, -0.24, -0.25, -0.23, -0.19, -0.18, -0.2};
#elif LENGTH_N == 512
double ECG[512] = {-0.245, -0.215, -0.185, -0.175, -0.17, -0.17, -0.185, -0.17, -0.16, -0.15, -0.17, -0.205, -0.22,
                   -0.23, -0.21, -0.19, -0.175, -0.185, -0.19, -0.19, -0.2, -0.2, -0.21, -0.205, -0.215, -0.205, -0.225,
                   -0.235, -0.225, -0.205, -0.21, -0.2, -0.2, -0.195, -0.185, -0.19, -0.215, -0.22, -0.195, -0.195,
                   -0.22, -0.25, -0.22, -0.165, -0.15, -0.145, -0.185, -0.195, -0.195, -0.185, -0.17, -0.16, -0.16,
                   -0.18, -0.205, -0.2, -0.165, -0.135, -0.13, -0.13, -0.13, -0.11, -0.065, -0.03, -0.04, -0.055,
                   -0.055, -0.035, 0.0, 0.015, 0.015, 0.0, 0.01, 0.015, 0.02, 0.035, 0.045, 0.04, 0.03, 0.025, 0.005,
                   -0.005, -0.025, -0.05, -0.05, -0.045, -0.025, -0.02, -0.035, -0.065, -0.065, -0.075, -0.07, -0.085,
                   -0.1, -0.14, -0.145, -0.14, -0.115, -0.095, -0.09, -0.1, -0.11, -0.125, -0.125, -0.11, -0.085,
                   -0.095, -0.08, -0.065, -0.03, -0.04, -0.065, -0.11, -0.145, -0.145, -0.075, 0.035, 0.22, 0.435,
                   0.695, 1.005, 1.3, 1.535, 1.72, 1.82, 1.72, 1.425, 1.02, 0.6, 0.2, -0.095, -0.215, -0.19, -0.125,
                   -0.105, -0.115, -0.13, -0.1, -0.075, -0.08, -0.11, -0.125, -0.155, -0.165, -0.17, -0.17, -0.17,
                   -0.17, -0.185, -0.185, -0.2, -0.19, -0.175, -0.155, -0.14, -0.175, -0.175, -0.17, -0.15, -0.16,
                   -0.165, -0.18, -0.19, -0.165, -0.14, -0.13, -0.15, -0.16, -0.145, -0.125, -0.115, -0.11, -0.115,
                   -0.12, -0.115, -0.095, -0.085, -0.09, -0.09, -0.1, -0.095, -0.085, -0.065, -0.06, -0.07, -0.05,
                   -0.045, -0.02, 0.01, 0.02, 0.04, 0.055, 0.065, 0.075, 0.095, 0.115, 0.085, 0.06, 0.08, 0.125, 0.16,
                   0.165, 0.17, 0.185, 0.22, 0.225, 0.235, 0.22, 0.23, 0.25, 0.28, 0.27, 0.25, 0.255, 0.245, 0.235,
                   0.235, 0.25, 0.23, 0.245, 0.23, 0.22, 0.215, 0.18, 0.12, 0.075, 0.065, 0.06, 0.055, 0.025, -0.005,
                   -0.04, -0.085, -0.135, -0.145, -0.14, -0.12, -0.12, -0.16, -0.195, -0.2, -0.215, -0.2, -0.2, -0.215,
                   -0.255, -0.27, -0.225, -0.225, -0.24, -0.25, -0.23, -0.19, -0.18, -0.2, -0.21, -0.215, -0.21, -0.21,
                   -0.205, -0.225, -0.23, -0.24, -0.245, -0.23, -0.23, -0.21, -0.2, -0.185, -0.175, -0.175, -0.19,
                   -0.19, -0.19, -0.2, -0.175, -0.13, -0.09, -0.095, -0.13, -0.17, -0.16, -0.135, -0.1, -0.09, -0.115,
                   -0.115, -0.08, -0.04, -0.065, -0.105, -0.12, -0.12, -0.11, -0.105, -0.11, -0.15, -0.155, -0.16,
                   -0.15, -0.165, -0.19, -0.215, -0.27, -0.32, -0.34, -0.32, -0.28, -0.285, -0.31, -0.36, -0.375, -0.34,
                   -0.31, -0.31, -0.305, -0.32, -0.32, -0.35, -0.35, -0.325, -0.3, -0.32, -0.38, -0.395, -0.335, -0.275,
                   -0.26, -0.29, -0.33, -0.335, -0.33, -0.325, -0.27, -0.145, 0.045, 0.28, 0.535, 0.83, 1.105, 1.335,
                   1.5, 1.51, 1.27, 0.865, 0.41, 0.0, -0.255, -0.33, -0.33, -0.315, -0.325, -0.32, -0.27, -0.26, -0.29,
                   -0.335, -0.33, -0.345, -0.35, -0.335, -0.305, -0.3, -0.34, -0.36, -0.37, -0.38, -0.38, -0.37, -0.35,
                   -0.325, -0.305, -0.295, -0.275, -0.305, -0.325, -0.345, -0.335, -0.335, -0.335, -0.36, -0.365, -0.35,
                   -0.38, -0.41, -0.395, -0.385, -0.36, -0.34, -0.345, -0.325, -0.3, -0.295, -0.285, -0.29, -0.295,
                   -0.285, -0.275, -0.275, -0.235, -0.225, -0.215, -0.19, -0.125, -0.055, -0.04, -0.065, -0.095, -0.125,
                   -0.095, -0.095, -0.07, -0.045, -0.01, -0.02, -0.015, -0.01, -0.03, -0.025, 0.005, 0.065, 0.1, 0.11,
                   0.06, -0.05, -0.13, -0.08, 0.04, 0.125, 0.115, 0.045, -0.02, -0.025, 0.02, 0.015, -0.07, -0.135,
                   -0.165, -0.165, -0.1, -0.04, -0.085, -0.22, -0.44, -0.615, -0.66, -0.615, -0.55, -0.505, -0.48,
                   -0.45, -0.395, -0.355, -0.32, -0.305, -0.32, -0.37, -0.51, -0.67, -0.725, -0.58, -0.395, -0.325,
                   -0.37, -0.42, -0.47, -0.47, -0.435, -0.335, -0.18, -0.035, 0.01, -0.135, -0.34, -0.495, -0.575,
                   -0.56, -0.52, -0.505, -0.445, -0.36, -0.33, -0.38, -0.415, -0.41, -0.415, -0.46, -0.465, -0.41,
                   -0.36, -0.33, -0.31, -0.3, -0.255, -0.215, -0.235, -0.34, -0.4, -0.385, -0.325, -0.275, -0.295,
                   -0.365, -0.365, -0.325, -0.31, -0.335, -0.36, -0.37, -0.345, -0.33};
#endif

double M[LENGTH_N] = {0}; // Reason why it's LENGTH_N and not LENGTH_M is cuz of the 8-bit one

double sin1;
double cos1;
double sin3;
double cos3;
double r2sin6;
double r2cos6;

/*---------------------------------------------------------------------------*/
PROCESS(main_proc, "Calculation Process");
AUTOSTART_PROCESSES(&main_proc);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(main_proc, ev, data) {

    PROCESS_BEGIN();
                energest_init();

                // Precomputing SIN and COS values for 8-Point DCT to **SIGNIFICANTLY** reduce the number of complex calculations
                // The constants were taken from: https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.463.3353&rep=rep1&type=pdf
                sin1 = sin(1 * M_PI / 16);
                cos1 = cos(1 * M_PI / 16);
                sin3 = sin(3 * M_PI / 16);
                cos3 = cos(3 * M_PI / 16);
                r2sin6 = sqrt(2) * sin(6 * M_PI / 16);
                r2cos6 = sqrt(2) * cos(6 * M_PI / 16);


                printf("\n// ======= NAIVE DCT ======= //\n");

                energest_flush();
                long unsigned int pre_cpu = energest_type_time(ENERGEST_TYPE_CPU);

                u_long start_time = clock_seconds();
                dct_slow(LENGTH_N, LENGTH_M);
                u_long end_time = clock_seconds();

                energest_flush();
                long unsigned int post_cpu = energest_type_time(ENERGEST_TYPE_CPU);
                u_long dct_slow_period = end_time - start_time;

                printf("NAIVE DCT TIME: %lu\n", dct_slow_period);
                print_power_consumption(pre_cpu, post_cpu);
                print_coeffs(LENGTH_M);

                printf("// ========================= //\n");

//              =========================================================

                printf("\n// ======= FASTER DCT ======= //\n");
                energest_flush();
                pre_cpu = energest_type_time(ENERGEST_TYPE_CPU);

                start_time = clock_seconds();
                faster_dct(LENGTH_N, LENGTH_M);
                end_time = clock_seconds();

                energest_flush();
                post_cpu = energest_type_time(ENERGEST_TYPE_CPU);
                u_long dct_faster_period = end_time - start_time;

                printf("FASTER DCT TIME: %lu\n", dct_faster_period);
                print_power_consumption(pre_cpu, post_cpu);
                print_coeffs(LENGTH_M);
                printf("// ========================= //\n");

//              =========================================================

                printf("\n// ========= LLM DCT ======== //\n");
                energest_flush();
                pre_cpu = energest_type_time(ENERGEST_TYPE_CPU);

                start_time = clock_seconds();
                small_dct(LENGTH_N);
                end_time = clock_seconds();

                energest_flush();
                post_cpu = energest_type_time(ENERGEST_TYPE_CPU);
                u_long llm_dct_period = end_time - start_time;

                printf("LLM DCT TIME: %lu\n", llm_dct_period);
                print_power_consumption(pre_cpu, post_cpu);
                print_coeffs(LENGTH_N);
                printf("// ========================= //");

    PROCESS_END();
}


void dct_slow(int n, int m) {
    int i;
    int j;
    double c;
    for (i = 0; i < m; ++i) {
        M[i] = 0;
        if (i == 0) {
            c = 1 / sqrt(n);
        } else {
            c = sqrt(2.0 / n);
        }
        for (j = 0; j < n; ++j) {
            double cos_ = cos((2 * M_PI * i * (2 * j + 1)) / (4 * n));
            M[i] += cos_ * ECG[j];
        }
        M[i]*=c;
    }
}


void faster_dct(int n, int m) {
    int i;
    int j;
    double c;

    for (i = 0; i < m; ++i) {
        M[i] = 0;
        if (i == 0) {
            c = 1 / sqrt(n);
        } else {
            c = sqrt(2.0 / n);
        }
        for (j = 0; j < n/2; ++j) {
            double cos_ = cos((2 * M_PI * i * (2 * j + 1)) / (4 * n));
            M[i] += cos_ * ECG[j];
            if (i % 2 == 0) {
                M[i] += cos_ * ECG[n - 1 - j];
            } else {
                M[i] += -cos_ * ECG[n - 1 - j];
            }
        }
        M[i]*=c;
    }
}

void small_dct(int n) {
    int i;
    for (i = 0; i < n / 8; i++) {
        llm(i);
    }

}

void llm(int i) {
    double root2 = sqrt(2);
    double root8 = sqrt(8);
    int inc = 8 * i;

    double a0 = ECG[0 + inc] + ECG[7 + inc];
    double a1 = ECG[1 + inc] + ECG[6 + inc];
    double a2 = ECG[2 + inc] + ECG[5 + inc];
    double a3 = ECG[3 + inc] + ECG[4 + inc];
    double a4 = ECG[3 + inc] - ECG[4 + inc];
    double a5 = ECG[2 + inc] - ECG[5 + inc];
    double a6 = ECG[1 + inc] - ECG[6 + inc];
    double a7 = ECG[0 + inc] - ECG[7 + inc];

    double b0 = a0 + a3;
    double b1 = a1 + a2;
    double b2 = a1 - a2;
    double b3 = a0 - a3;

    double z1 = cos3 * (a7 + a4);
    double b4 = (sin3 - cos3) * a7 + z1;
    double b7 = (-sin3 - cos3) * a4 + z1;

    double z2 = cos1 * (a6 + a5);
    double b5 = (sin1 - cos1) * a6 + z2;
    double b6 = (-sin1 - cos1) * a5 + z2;

    double c0 = b0 + b1;
    double c1 = -b1 + b0;

    double z3 = r2cos6 * (b3 + b2);
    double c2 = (r2sin6 - r2cos6) * b3 + z3;
    double c3 = (-r2sin6 - r2cos6) * b2 + z3;

    double c4 = b4 + b6;
    double c5 = b7 - b5;
    double c6 = b4 - b6;
    double c7 = b7 + b5;

    double d4 = c7 - c4;
    double d5 = c5 * root2;
    double d6 = c6 * root2;
    double d7 = c7 + c4;

    M[0 + inc] = c0 / root8;
    M[1 + inc] = d7 / root8;
    M[2 + inc] = c2 / root8;
    M[3 + inc] = d5 / root8;
    M[4 + inc] = c1 / root8;
    M[5 + inc] = d6 / root8;
    M[6 + inc] = c3 / root8;
    M[7 + inc] = d4 / root8;
}

void print_double(double val) {
    if (val >= 1. || val <= -1.) {
        int A;
        double frac;
        A = val;
        frac = fabs((val - A) * 1000);
        printf("%d.%02u", A, (unsigned int) frac);
    } else if (val >= 0. && val < 1.) {
        printf("0.%03d", (int) (val * 1000));
    } else if (val > -1.0 && val < 0.) {
        printf("-0.%03d", (int) (val * (-1000)));
    }
}

void print_coeffs(int m) {
    int i;
    printf("COEFFICIENTS: [");
    for (i = 0; i < m; i++) {
        print_double(M[i]);
        printf(", ");
    }
    printf("]\n");
}

void print_power_consumption(long unsigned int cpu_pre, long unsigned int cpu_post){
    long double power_consumption = ((long double)(cpu_post-cpu_pre)/RTIMER_ARCH_SECOND)*CURRENT*VOLTAGE;
    printf("POWER CONSUMPTION: ");
    print_double(power_consumption);
    printf(" mJ\n");
}
# As with the C code, I am fully aware this code's kinda nasty, but I don't think the point of the exercise was to
# make the cleanest code so this'll do.

import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
from scipy.fftpack import dct
from scipy.misc import electrocardiogram

ecg = electrocardiogram()
sns.set()

# %%

print(*ecg[:512], sep=",")
print(*ecg[:256], sep=",")

# %%
llm_dct = [-0.535, -0.053, -0.040, -0.019, -0.001, -0.009, 0.007, -0.002, -0.542, 0.057, 0.042, -0.026, -0.005, -0.009,
           -0.004, 0.003, -0.549, 0.028, 0.003, 0.002, 0.005, 0.001, 0.004, -0.004, -0.608, -0.011, 0.023, 0.005,
           -0.010, -0.012, 0.001, -0.006, -0.563, 0.009, 0.007, -0.026, -0.001, 0.012, -0.005, 0.001, -0.540, -0.061,
           -0.059, 0.036, 0.024, 0.011, 0.013, -0.008, -0.514, 0.013, -0.042, -0.004, 0.008, -0.007, 0.004, -0.000,
           -0.316, -0.100, 0.037, -0.039, -0.005, -0.005, -0.006, -0.002, -0.054, -0.071, -0.002, 0.034, 0.001, 0.001,
           -0.000, 0.003, 0.077, -0.020, -0.023, 0.007, 0.003, -0.002, -0.001, -0.000, -0.076, 0.026, 0.046, -0.001,
           -0.005, -0.000, -0.002, 0.005, -0.224, 0.071, -0.016, 0.031, -0.001, 0.011, 0.001, 0.011, -0.325, -0.026,
           -0.045, 0.003, 0.003, 0.005, -0.000, 0.002, -0.222, -0.081, 0.006, -0.013, -0.017, 0.007, -0.002, 0.012,
           0.053, -0.439, 0.314, -0.067, 0.053, -0.023, 0.011, 0.002, 3.96, -0.817, -0.599, 0.072, -0.166, 0.020,
           -0.034, 0.001, 0.385, 0.973, 0.654, 0.172, 0.042, 0.044, 0.019, 0.011, -0.314, 0.020, -0.061, 0.010, 0.014,
           0.016, -0.001, 0.006, -0.500, 0.027, -0.011, 0.005, 0.001, 0.003, -0.004, 0.005, -0.470, -0.012, -0.014,
           -0.035, 0.007, 0.007, 0.002, -0.011, -0.452, -0.033, 0.025, 0.025, -0.017, 0.000, -0.002, 0.004, -0.355,
           -0.035, -0.027, -0.009, 0.001, -0.003, 0.001, 0.000, -0.249, -0.012, 0.016, -0.017, 0.001, -0.003, -0.001,
           -0.005, -0.061, -0.105, 0.018, 0.003, 0.001, 0.000, 0.013, 0.002, 0.222, -0.014, -0.041, -0.000, 0.021,
           -0.018, 0.001, 0.001, 0.525, -0.097, 0.002, -0.008, -0.019, -0.010, 0.000, -0.008, 0.707, -0.021, -0.042,
           -0.010, 0.010, 0.009, -0.009, -0.000, 0.657, 0.020, -0.016, 0.001, -0.003, -0.006, 0.006, 0.013, 0.203,
           0.136, 0.025, 0.056, 0.008, 0.008, 0.002, -0.002, -0.334, 0.068, 0.048, 0.055, -0.008, 0.012, -0.006, -0.001,
           -0.618, 0.059, -0.034, 0.025, 0.007, -0.006, -0.000, -0.007, -0.615, -0.046, 0.030, 0.024, -0.024, 0.001,
           -0.001, -0.000, ]
llm_dct_512 = [-0.535, -0.053, -0.040, -0.019, -0.001, -0.009, 0.007, -0.002, -0.542, 0.057, 0.042, -0.026, -0.005,
               -0.009, -0.004, 0.003, -0.549, 0.028, 0.003, 0.002, 0.005, 0.001, 0.004, -0.004, -0.608, -0.011, 0.023,
               0.005, -0.010, -0.012, 0.001, -0.006, -0.563, 0.009, 0.007, -0.026, -0.001, 0.012, -0.005, 0.001, -0.540,
               -0.061, -0.059, 0.036, 0.024, 0.011, 0.013, -0.008, -0.514, 0.013, -0.042, -0.004, 0.008, -0.007, 0.004,
               -0.000, -0.316, -0.100, 0.037, -0.039, -0.005, -0.005, -0.006, -0.002, -0.054, -0.071, -0.002, 0.034,
               0.001, 0.001, -0.000, 0.003, 0.077, -0.020, -0.023, 0.007, 0.003, -0.002, -0.001, -0.000, -0.076, 0.026,
               0.046, -0.001, -0.005, -0.000, -0.002, 0.005, -0.224, 0.071, -0.016, 0.031, -0.001, 0.011, 0.001, 0.011,
               -0.325, -0.026, -0.045, 0.003, 0.003, 0.005, -0.000, 0.002, -0.222, -0.081, 0.006, -0.013, -0.017, 0.007,
               -0.002, 0.012, 0.053, -0.439, 0.314, -0.067, 0.053, -0.023, 0.011, 0.002, 3.966, -0.817, -0.599, 0.072,
               -0.166, 0.020, -0.034, 0.001, 0.385, 0.973, 0.654, 0.172, 0.042, 0.044, 0.019, 0.011, -0.314, 0.020,
               -0.061, 0.010, 0.014, 0.016, -0.001, 0.006, -0.500, 0.027, -0.011, 0.005, 0.001, 0.003, -0.004, 0.005,
               -0.470, -0.012, -0.014, -0.035, 0.007, 0.007, 0.002, -0.011, -0.452, -0.033, 0.025, 0.025, -0.017, 0.000,
               -0.002, 0.004, -0.355, -0.035, -0.027, -0.009, 0.001, -0.003, 0.001, 0.000, -0.249, -0.012, 0.016,
               -0.017, 0.001, -0.003, -0.001, -0.005, -0.061, -0.105, 0.018, 0.003, 0.001, 0.000, 0.013, 0.002, 0.222,
               -0.014, -0.041, -0.000, 0.021, -0.018, 0.001, 0.001, 0.525, -0.097, 0.002, -0.008, -0.019, -0.010, 0.000,
               -0.008, 0.707, -0.021, -0.042, -0.010, 0.010, 0.009, -0.009, -0.000, 0.657, 0.020, -0.016, 0.001, -0.003,
               -0.006, 0.006, 0.013, 0.203, 0.136, 0.025, 0.056, 0.008, 0.008, 0.002, -0.002, -0.334, 0.068, 0.048,
               0.055, -0.008, 0.012, -0.006, -0.001, -0.618, 0.059, -0.034, 0.025, 0.007, -0.006, -0.000, -0.007,
               -0.615, -0.046, 0.030, 0.024, -0.024, 0.001, -0.001, -0.000, -0.616, 0.024, -0.018, 0.005, 0.005, 0.000,
               -0.002, 0.007, -0.583, -0.070, -0.002, 0.001, -0.003, -0.001, -0.006, -0.005, -0.445, -0.107, 0.049,
               0.006, -0.021, 0.006, -0.001, 0.005, -0.358, -0.053, -0.011, 0.043, 0.019, 0.001, 0.014, 0.001, -0.263,
               0.058, 0.025, -0.027, -0.026, -0.015, -0.008, -0.001, -0.457, 0.069, -0.010, 0.037, 0.008, 0.006, 0.006,
               0.008, -0.878, 0.020, -0.014, 0.076, 0.008, 0.007, -0.003, 0.008, -0.929, -0.018, -0.056, -0.011, -0.017,
               0.001, -0.007, 0.009, -0.947, -0.000, 0.041, -0.095, 0.010, 0.008, -0.001, -0.000, -0.807, -0.066, 0.138,
               -0.042, 0.051, -0.024, 0.005, -0.005, 2.524, -1.474, -0.192, -0.021, -0.056, -0.001, -0.031, -0.001,
               0.464, 1.504, 0.646, 0.108, 0.030, 0.032, 0.021, 0.005, -0.875, 0.034, -0.064, -0.032, 0.012, 0.019,
               -0.002, 0.011, -0.968, 0.048, -0.049, -0.029, 0.000, 0.013, 0.001, -0.003, -0.921, -0.093, -0.025, 0.010,
               -0.008, 0.012, -0.010, 0.006, -0.972, 0.027, -0.005, -0.003, 0.021, -0.000, 0.005, 0.007, -1.39, -0.071,
               0.014, 0.017, 0.014, 0.000, 0.016, 0.002, -0.813, -0.020, 0.004, -0.009, -0.007, 0.004, 0.001, 0.000,
               -0.406, -0.211, 0.007, 0.043, -0.028, 0.000, 0.000, 0.000, -0.196, -0.100, 0.024, 0.011, -0.001, 0.020,
               0.007, 0.016, 0.070, -0.136, 0.063, 0.013, -0.017, -0.002, -0.003, -0.006, 0.044, -0.143, 0.080, 0.180,
               0.001, 0.010, -0.004, 0.000, -0.192, 0.180, -0.074, -0.052, 0.022, 0.022, 0.009, -0.001, -0.981, 0.678,
               -0.025, -0.167, 0.008, -0.017, -0.010, -0.009, -1.187, -0.237, -0.002, -0.007, -0.014, -0.012, -0.006,
               0.002, -1.377, 0.037, 0.408, -0.049, -0.065, 0.018, -0.006, -0.004, -0.959, -0.304, 0.270, -0.039, 0.033,
               -0.003, 0.000, -0.004, -1.103, 0.481, 0.312, 0.046, -0.003, 0.008, -0.003, -0.004, -1.136, 0.055, -0.057,
               -0.048, -0.065, -0.000, -0.004, 0.002, -0.935, -0.205, -0.019, -0.053, 0.001, -0.007, -0.005, 0.003,
               -0.926, 0.004, 0.058, 0.136, -0.00, 0.021, 0.002, 0.002, -0.968, 0.010, 0.001, -0.052, -0.014, -0.003,
               -0.004, 0.003]
faster_256_70 = np.zeros(256)
faster_512_120 = np.zeros(512)

faster_256_70[0:75] = [-0.397, -0.863, -1.340, -0.446, 0.365, 1.334, -1.886, 0.415, 1.167, 0.439, -1.265, -0.802, 1.259,
                       0.538, -0.781, -0.683, 0.904, 0.559, -0.799, -0.727, 0.687, 0.736, -0.618, -0.718, 0.545, 0.700,
                       -0.471, -0.747, 0.351, 0.713, -0.288, -0.747, 0.310, 0.614, -0.165, -0.611, 0.094, 0.471, -0.164,
                       -0.505, 0.087, 0.367, -0.048, -0.385, 0.007, 0.295, 0.006, -0.238, 0.010, 0.164, -0.040, -0.135,
                       0.024, 0.135, -0.092, -0.096, 0.076, 0.081, -0.002, -0.098, 0.106, 0.055, -0.050, -0.100, 0.039,
                       0.083, -0.039, -0.053, 0.024, 0.068, -0.063, -0.024, 0.016, 0.040, -0.099]

faster_512_120[0:120] = [-2.674, 2.07, -1.388, -0.714, -1.510, -0.059, -0.312, -0.304, -0.115, 1.496, 1.103, -1.285,
                         0.005, -1.862, 0.200, 2.06, -0.234, 0.686, 0.941, -0.593, -1.359, -0.275, -0.866, -0.284,
                         1.911, 0.248, 0.158, 0.491, -0.880, -1.31, 0.153, -0.217, 0.242, 1.628, -0.293, -0.147, -0.015,
                         -1.143, -0.706, 0.622, 0.012, 0.557, 1.122, -0.376, -0.645, -0.009, -0.970, -0.236, 0.978,
                         0.180, 0.394, 0.541, -0.554, -0.913, 0.143, -0.688, 0.233, 0.943, 0.138, 0.161, 0.170, -0.756,
                         -0.787, 0.433, -0.335, 0.564, 0.762, -0.182, -0.041, -0.132, -0.649, -0.385, 0.541, -0.153,
                         0.449, 0.366, -0.338, -0.378, -0.064, -0.423, -0.026, 0.620, -0.122, 0.256, 0.133, -0.408,
                         -0.258, -0.006, -0.184, 0.199, 0.334, 0.010, -0.058, 0.177, -0.539, 0.100, 0.029, -0.004,
                         0.113, 0.194, -0.141, -0.163, 0.176, -0.395, 0.292, -0.070, 0.145, 0.019, -0.017, -0.213,
                         0.015, 0.039, -0.082, 0.190, 0.027, -0.002, 0.035, -0.029, -0.180, 0.157]


def get_inv(llm_dct):
    ecg_rev = []
    for i in range(0, int(len(llm_dct) / 8)):
        ecg_rev.append(dct(llm_dct[8 * i:8 * (i + 1)], type=3, norm='ortho'))
    ecg_rev = [val for sub in ecg_rev for val in sub]
    return ecg_rev


sns.lineplot(data=ecg[:256], label="ECG")
sns.lineplot(data=get_inv(llm_dct), label="ECG after DCT (LLM)")
plt.show()

sns.lineplot(data=ecg[:256], label="ECG")
sns.lineplot(data=dct(faster_256_70, type=3, norm='ortho'), label="ECG after DCT (NAIVE)")
plt.show()


print(f"MSE of 8-bit LLM DCT (n=256; m=256): {((np.array(ecg[:256]) - np.array(get_inv(llm_dct))) ** 2).mean(axis=0)}")
print(
    f"MSE of 8-bit LLM DCT (n=512; m=512): {((np.array(ecg[:512]) - np.array(get_inv(llm_dct_512))) ** 2).mean(axis=0)}")

print(
    f"MSE of NAIVE / FASTER DCT (256; 75): {((np.array(ecg[:256]) - np.array(dct(faster_256_70, type=3, norm='ortho'))) ** 2).mean(axis=0)}")
print(
    f"MSE of NAIVE / FASTER DCT (512; 120): {((np.array(ecg[:512]) - np.array(dct(faster_512_120, type=3, norm='ortho'))) ** 2).mean(axis=0)}")